from django.urls import path
from jobs.api.views import JobOfferListAPIView,  JobOfferDetailAPIView

urlpatterns = [
    path("joboffers/", JobOfferListAPIView.as_view(), name="joboffer-list"),
    path("joboffers/<int:pk>/", JobOfferDetailAPIView.as_view(), name="joboffer-detail"),
]
