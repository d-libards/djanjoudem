from django.db import models

# Create your models here.


class Quote(models.Model):
    qoute_author = models.CharField(max_length=60)
    qoute_body = models.TextField()
    context = models.CharField(max_length=240, blank=True)
    source = models.CharField(max_length=240, blank=True)

    def __str__(self):
        return self.qoute_author
