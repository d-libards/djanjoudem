# Generated by Django 5.0.3 on 2024-03-20 01:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('qoute_author', models.CharField(max_length=60)),
                ('qoute_body', models.TextField()),
                ('context', models.CharField(blank=True, max_length=240)),
                ('source', models.CharField(blank=True, max_length=240)),
            ],
        ),
    ]
