from django.http import JsonResponse
from .models import Product, Manufacturer


def manufacturers_list(request):
    manufacturers = Manufacturer.objects.filter(active=True)
    data = {"manufacturers": list(manufacturers.values())}
    response = JsonResponse(data)
    return response


def manufacturers_detail(request, pk):
    try:
        manufacturer = Manufacturer.objects.get(pk=pk)
        manufacturer_products = manufacturer.products.all()
        data = {"manufacturer": {
            "name": manufacturer.name,
            "location": manufacturer.location,
            "description": manufacturer.active,
            "products": list(manufacturer_products.values())
            }}
        response = JsonResponse(data)
    except Product.DoesNotExist:
        response = JsonResponse({
            "Error": {
                "code": 404,
                "message": "Product not found!",
            }},
            status=404)
    return response


def product_list(request):
    products = Product.objects.all()
    data = {"products": list(products.values())}  #"pk", "name"
    response = JsonResponse(data)
    return response


def product_detail(request, pk):
    try:
        product = Product.objects.get(pk=pk)
        data = {"product": {
            "name": product.name,
            "manufacturer": product.manufacturer.name,
            "description": product.description,
            "photo": product.photo.url,
            "shipping_cost": product.shipping_cost,
            "price": product.price,
            "quantity": product.quantity,

            }}
        response = JsonResponse(data)
    except Product.DoesNotExist:
        response = JsonResponse({
            "Error": {
                "code": 404,
                "message": "Product not found!",
            }},
            status=404)
    return response


'''
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView


class ProductDetailView(DetailView):
    model = Product
    template_name = "products/product_detail.html"


class ProductListView(ListView):
    model = Product
    template_name = "products/product_list.html"
'''