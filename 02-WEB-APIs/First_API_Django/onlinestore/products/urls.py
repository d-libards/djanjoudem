from django.urls import path
from .views import product_list, product_detail, manufacturers_list, manufacturers_detail


urlpatterns = [
    path("products/", product_list, name="product-list"),
    path("products/<int:pk>/", product_detail, name="product-detail"),
    path("manufacturers/", manufacturers_list, name="manufacturers-list"),
    path("manufacturers/<int:pk>/", manufacturers_detail, name="manufacturers-detail"),
]
