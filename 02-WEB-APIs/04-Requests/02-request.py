import requests


def main():

    base_currency = "USD"
    target_currency = "SEK"

    response = requests.get(f"https://v6.exchangerate-api.com/v6/6472d6d1551601067a789c4b/pair/{base_currency}/{target_currency}")
    if response.status_code != 200:
        print("Status Code:", response.status_code)
        raise Exception("There was an error!")
    data = response.json()
    print("JSON Data: ", data)


if __name__ == "__main__":
    main()
