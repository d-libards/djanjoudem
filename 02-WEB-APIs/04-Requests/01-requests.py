import requests


def main():
    response = requests.get("https://v6.exchangerate-api.com/v6/6472d6d1551601067a789c4b/latest/USD")
    if response.status_code != 200:
        print("Status Code:", response.status_code)
        raise Exception("There was an error!")
    data = response.json()
    print("JSON Data: ", data)


if __name__ == "__main__":
    main()
